#!/bin/bash
# script modified by Dasche.

#where you want the coin to be installed
# Edit these variables to get proper install.

BASE='/home/sap/'
PORT=7555
TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='methuselah.conf'
COIN_NAME='Methuselah'
COIN_DAEMON='/usr/local/bin/methuselahd'
COIN_CLI='/usr/local/bin/methuselah-cli'
COIN='sap'

# Execute options
ARGS=$(getopt -o "hp:n:c:r:wsudx" -l "help,count:,net" -n "multinode_$COIN.sh" -- "$@");
#edit these to set network to ipv4 (net=4)  (otherwise pass parameters -net or -n # on script install execute )
net=0
#******************
# edit for count of number of masternodes to install. (otherwise pass parameters -count or -c # on script install execute )
count=0
# ******************

eval set -- "$ARGS";
#take input from command line
while true; do
    case "$1" in
#set variable for -n or --net input
        -n|--net)
            shift;
                    if [ -n "$1" ];
                    then
                        net="$1";
                        shift;
                    fi
            ;;
#set variable for -c or --count input
        -c|--count)
            shift;
                    if [ -n "$1" ];
                    then
                        count="$1";
                        shift;
                    fi
            ;;
        --)
           shift;
            break;
            ;;
   esac done
# if no arguments are sent continue
if [ "$count" == 0 ]; then
        count=1

else
echo 'Installing '$count' masternodes using ipv'$net
fi
#######################-------IP TESTING
# break here of net detection is invalid
if [ "$net" -ne 4 ] && [ "$net" -ne 6 ]; then
    echo "invalid NETWORK setting, can only be 4 or 6!"
    exit 1;
fi


# use IPv4
if [ "$net" = 4 ]; then
        IPADDRESS=$(ip addr | grep 'inet ' | grep -Ev 'inet 127|inet 192\.168|inet 10\.' | sed "s/[[:space:]]*inet \([0-9.]*\)\/.*/\1/")
fi

# use IPv6
if [ "$net" = 6 ]; then
#step 1 - get the actual ipv6 address
        IPADDRESS=$(ip -6 addr show | grep inet6 | awk -F '[ \t]+|/' '{print $3}' | grep -v ^fe80 | grep -v ^::1 | cut -f1-5 -d':' | head -1)
#step 2 - assemble at least 1 supernet
fi
#######################--------END IP TESTING


# currently only for Ubuntu 16.04 & 18.04

if [[ -r /etc/os-release ]]; then
    VERSION_ID=$(cat /etc/os-release | grep VERSION_ID | cut -c 13- | cut -c-5)
    if [[ "$VERSION_ID"} = '16.04' ]] && [[ "$VERSION_ID" = '18.04' ]] ; then
#false
    echo 'the version '$VERSION_ID 'does not appear to match 16.04 or 18.04 - Exiting!'
    exit 1
    else
#true
    echo 'This script will function on this release of Ubuntu version '$VERSION_ID
    fi
fi


#Check Deps

echo '===[ Checking Dependancies ]==============================================='

if [ -d "/var/lib/fail2ban/" ]; then
    echo -e "Dependencies already installed..."
    else
    echo -e "Updating system and installing required packages..."
 DEBIAN_FRONTEND=noninteractive
  apt-get -y update
  apt-get -y autoremove
  apt-get -y install wget nano htop jq
  apt-get install unzip
echo 'Installing and starting fail2ban anti-hacking system'
 apt-get -y install fail2ban
 service fail2ban restart
fi
echo '==========================================================================='
sleep 2
clear

#Create 4GB swap file
echo '===[Swap configuration]===================================================='
echo "*** Testing for Swap file or partition"
    if grep -q "SwapTotal" /proc/meminfo; then
    echo -e "[SWAP FILE OK]Skipping disk swap configuration... \n"
    else
    echo "There is no swap file \n"
    echo -e "Creating 4GB disk swap file. \nThis may take a few minutes! \a"
    touch /var/swap.img
    chmod 600 swap.img
    dd if=/dev/zero of=/var/swap.img bs=1024k count=4000
    mkswap /var/swap.img 2> /dev/null
    swapon /var/swap.img 2> /dev/null
    if [ $? -eq 0 ]; then
        echo '/var/swap.img none swap sw 0 0' >> /etc/fstab
        echo -e "Swap was created successfully! \n"
    else
        echo -e "Swap Operation not permitted! \a"
        rm /var/swap.img
    fi
  fi
echo '==========================================================================='
sleep 2
clear
echo '=====[ Downloading masternode Files ]======================================'
echo 'Finding querying for latest version' && wget -q https://raw.githubusercontent.com/methuselah-coin/version/master/stable
echo 'Downloading and installing '$COIN_NAME' masternode files.. '
sleep 1
version=$(head -n 1 stable)
#echo "$version"

# [update routine needs work, for now install regardless if update is needed or not]

#Download Latest
echo '=====[ Install Latest Version of Masternode ]=============================='
echo 'Downloading latest version :'$version
wget -q https://github.com/methuselah-coin/methuselah/releases/download/version/methuselah-"$version"-linux.tar.xz
echo 'Extracting new '$COIN_NAME' files'
tar -xf methuselah-$version-linux.tar.xz -C /usr/local/bin 1> /dev/null
echo '[#####                  ]   (33%)'
sleep 1

rm methuselah-$version-linux.tar.xz
rm stable
echo '[#############          ]   (66%)'
sleep 1

 strip $COIN_DAEMON $COIN_CLI &> /dev/null
 cd ~ &> /dev/null
 chmod +x $COIN_CLI &> /dev/null
 chmod +x $COIN_DAEMON &> /dev/null

echo '[#######################]   (100%)'
sleep 2
echo $COIN' Masternode files are now installed'
echo '==========================================================================='
sleep 2
# Check and ADDUSER .
clear
echo '=====[ Creating user account [security] ]=================================='
echo 'Checking if '$COIN' user account exists'
        if getent passwd | grep $COIN >> /dev/null; then
                echo "user exists already...GOOD"
echo '==========================================================================='

        else
                echo "User is being created."
                sleep 1
                echo  '[#                      ]   (10%)'
                sleep 1
                echo  '[###                    ]   (21%)'
                sleep 1
                echo  '[#####                  ]   (33%)'
                adduser --disabled-password --gecos "" $COIN
                sleep 1
                echo  '[#############          ]   (66%)'
                sleep 1
                echo  '[#######################]   (100%)'
                echo 'USER ['$COIN'] is active!'
echo '==========================================================================='
fi
clear
#SETUP NETWORK
echo '======[ Setting up network ]==============================================='
echo 'Entering network detection phase'
IFACE=$(ip addr show | grep -m 1 global | tail -c 9 | cut -f1 -d':')
echo 'Network interface found on: '$IFACE
echo '==========================================================================='
sleep 2
clear
echo '======[Building Directories]==============================================='
echo '* Creating masternode directories'
mkdir -p $BASE
for num in $(seq 1 $count); do
    if [ ! -d $BASE"$COIN"$num ]; then
        echo 'Creating data directory ['${BASE}$COIN$num']'
        mkdir -p ${BASE}$COIN$num
echo '==========================================================================='
sleep 3
clear
echo '====[ Generating RPC Credientals ]========================================='
#SET RPC PASSWORD
    echo 'Generating Random Password for '$COIN' JSON RPC'
    USER=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    USERPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
echo '==========================================================================='
clear
echo 'Enter your masternode private key for Masternode# '$num
echo '[Press ENTER for blank] MasterNode Key: '
read Mkey
echo '==========================================================================='
sleep 2
clear
#MAKE CONF
echo 'Generating '$COIN'.conf'
echo "rpcuser=$USER
rpcpassword=$USERPASS
rpcallowip=127.0.0.1
rpcport=500$num
server=1
daemon=1
listen=1
promode=1
maxconnections=12
masternode=1
masternodeprivkey=$MKey
addnode=seed1.coinseed.org
addnode=node1.methuselahcoin.io:7555
addnode=node2.methuselahcoin.io:7555
addnode=node3.methuselahcoin.io:7555
addnode=node4.methuselahcoin.io:7555
addnode=node5.methuselahcoin.io:7555
addnode=node6.methuselahcoin.io:7555
addnode=node7.methuselahcoin.io:7555
addnode=node8.methuselahcoin.io:7555
addnode=node9.methuselahcoin.io:7555" | tee -a ${BASE}${COIN}$num/${COIN}.conf 1> /dev/null
sleep 2
clear
echo 'Masternode config updated for MN'$num
# for ipv6
if [ "$net" = 6 ]; then
echo 'bind=['$IPADDRESS':'$num]':'$PORT |tee -a ${BASE}${COIN}$num/${COIN}.conf 1>/dev/null
echo 'masternode IPV6 Bind Address added for masternode '$num ' >'$IPADDRESS':'$num
fi

# for ipv4
if [ "$net" = 4 ]; then
echo 'bind=192.168.1.'$num':'$PORT |tee -a ${BASE}${COIN}$num/${COIN}.conf 1>/dev/null
echo 'masternode IPV4 Bind Address added for masternode '$num ' > 192.168.1.'$num
fi
sleep 2
clear

#CREATE SYSTEMD FILE(S)
echo "[Unit]
Description=${COIN}$num Service
After=network.target

[Service]
User=root
Group=root
Type=forking

ExecStart=$COIN_DAEMON -daemon -conf=${BASE}${COIN}$num/${COIN}.conf -datadir=${BASE}${COIN}$num/
ExecStop=-$COIN_CLI -conf=${BASE}${COIN}$num/${COIN}.conf -datadir=${BASE}${COIN}$num/
stop

Restart=always
PrivateTmp=true
TimeoutStopSec=60s
TimeoutStartSec=10s
StartLimitInterval=120s
StartLimitBurst=5
[Install]
WantedBy=multi-user.target" | tee -a /etc/systemd/system/$COIN$num.service 1>/dev/null

#systemctl enable $COIN$num.service
echo '===[ Enabling automatic start at boot for systemd ]========================'
echo 'Systemd configured and enabled for autoboot for '$COIN$num'.service'
echo '==========================================================================='

sleep 2
clear
#configure IPV6 MULTINODE IP RANGE
if [ "$net" = 6 ]; then
echo '#Masternode '$num >> /etc/network/interfaces.tail
echo '  up   ip addr add '$IPADDRESS':'$num'/16 dev '$IFACE' label '$IFACE':'$num >> /etc/network/interfaces.mn
echo '  down ip addr del '$IPADDRESS':'$num'/16 dev '$IFACE' label '$IFACE':'$num >> /etc/network/interfaces.mn
fi

#configure IPV4 MULTINODE IP RANGE
if [ "$net" = 4 ]; then
echo '#Masternode '$num >> /etc/network/interfaces.tail
echo '  up   ip addr add 192.168.1.'$num'/24 dev '$IFACE' label '$IFACE':'$num >> /etc/network/interfaces.mn
echo '  down ip addr del 192.168.1.'$num'/24 dev '$IFACE' label '$IFACE':'$num >> /etc/network/interfaces.mn
fi
sleep 2
clear
echo '====[ Injecting multinode ip address ]====================================='
echo 'Multinode ip /etc/network/interfaces.tail created for MN'$num
echo '==========================================================================='
sleep 2
clear
fi
done
sleep 2
clear
echo 'Script complete, nodes will not be started automaticly; use systemctl start '${COIN}$num' for example'

touch /etc/network/interfaces.x | cat /etc/network/interfaces > /etc/network/interfaces.x | cat /etc/network/interfaces.x /etc/network/interfaces.mn >> /etc/network/interfaces | rm /etc/network/interfaces.x | systemctl restart networking




#restart networking

#ifdown $IFACE && ifup $IFACE