#------------------------------------
# edit to create USER's info
PASSWORD='enter your password'
USERNAME='enter your username'
SSHPUB='enter your RSA Pub key here, the full text in one line'
useradd -m -p ${PASSWORD} -s /bin/bash ${USERNAME}
usermod -a -G sudo ${USERNAME}
usermod -a -G ssh ${USERNAME}
sleep 1

echo -e "${PASSWORD}\n${PASSWORD}" | passwd ${USERNAME}
mkdir /home/${USERNAME}/.ssh/

chown ${USERNAME} /home/${USERNAME}/.ssh/
chgrp ${USERNAME} /home/${USERNAME}/.ssh/

#edit this line with your rsa public key.
echo  ${SSHPUB} > /home/${USERNAME}/.ssh/authorized_keys

chown ${USERNAME} /home/${USERNAME}/.ssh/authorized_keys
chgrp ${USERNAME} /home/${USERNAME}/.ssh/authorized_keys

sed -i -e 's/#force_color_prompt=yes/force_color_prompt=yes/g' /home/${USERNAME}/.bashrc
sed -i -e 's/#force_color_prompt=no/force_color_prompt=yes/g' /home/${USERNAME}/.bashrc
#--------------------------------------
# configure sshd_config to disable root and enable keys locations.
sed -i -e 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
# disable password authentication
sed -i -e 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
sed -i -e 's/#AuthorizedKeysFile/AuthorizedKeysFile/g' /etc/ssh/sshd_config
sed -i -e 's/X11Forwarding yes/X11Forwarding no/g' /etc/ssh/sshd_config
sed -i -e 's/PrintMotd no/PrintMotd yes/g' /etc/ssh/sshd_config
sed -i -e 's/#Banner /Banner /g' /etc/ssh/sshd_config
sed -i -e 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config

systemctl restart sshd
