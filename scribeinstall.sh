#--------------------------------------
#Scribe installer script
#--------------------------------------
# DATE OF CREATION: 4/23/2019
#!/bin/bash
# script developed by Dasche.



#Variables
COIN='scribe'
PORT=8800
COIN_BIN='https://github.com/scribenetwork/scribe/releases/download/v0.2/scribe-ubuntu-16.04-x64.tar.gz'
TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='scribe.conf'
COIN_NAME='Scribe'
COIN_DAEMON='/usr/local/bin/'${COIN}'d'
COIN_CLI='/usr/local/bin/'${COIN}'-cli'
BASE='/home'
IP=$(hostname --ip-address)
#--------------------------------------

function install_deps(){
echo '==[ Installing Dependancies and Repos ]===================================='
echo -ne '#                         (10%)\r'
sleep 1

# disable ipv6 for install session
sysctl -w net.ipv6.conf.all.disable_ipv6=1 > /dev/null 2>&1
echo -ne '#####                     (20%)\r'
sleep 1

# Set background install
 export DEBIAN_FRONTEND=noninteractive

# update repos
        apt-get -yq update > /dev/null 2>&1
        apt-get -yq install software-properties-common > /dev/null 2>&1
        apt-get -yq install libevent-dev > /dev/null 2>&1
        apt-get -yq install libevent1-dev > /dev/null 2>&1
        apt-get -yq install libevent-2.0 > /dev/null 2>&1
        apt-get -yq install git-core > /dev/null 2>&1

echo -ne '#######                     (33%)\r'
sleep 1

# install utiltities
        apt-get -yq autoremove > /dev/null 2>&1
        apt-get -yq install wget nano htop jq > /dev/null 2>&1
echo -ne '##########                  (56%)\r'
sleep 1

# install libboost
        apt-get -qq -y install libboost-all-dev > /dev/null 2>&1
echo -ne '##############              (69%)\r'
sleep 1

# install bitcoin repo for libdb 4.8 (wallet database)
cat <(echo "") | add-apt-repository ppa:bitcoin/bitcoin > /dev/null 2>&1

echo -ne '################            (74%)\r'
sleep 1

        apt-get -yq update > /dev/null 2>&1
echo -ne '###################         (91%)\r'
sleep 1

        apt-get -qy install build-essential libssl-dev libqrencode-dev libminiupnpc-dev gcc-6-base libdb4.8++ libdb4.8 libdb4.8-dev libdb4.8++-dev > /dev/null 2>&1

echo -ne '#########################   (100%)\r'
sleep 1
echo -ne '\n'

echo '==========================================================================='
}

function makeswp(){
#Create 4GB swap file (this may be slightly broken) 
echo '==========================================================================='
echo "*** Testing for Swap file \n"
    if grep -q "SwapTotal" /proc/meminfo; then
    echo -e "[SWAP FILE OK] Skipping disk swap configuration... \n"
    else
    echo "There is no swap file \n"
    echo -e "Creating 4GB disk swap file. \nThis may take a little time! \a"
    touch /var/swap.img
    chmod 600 swap.img
    dd if=/dev/zero of=/var/swap.img bs=1024k count=4000
    mkswap /var/swap.img 2> /dev/null
    swapon /var/swap.img 2> /dev/null
    if [ $? -eq 0 ]; then
        echo '/var/swap.img none swap sw 0 0' >> /etc/fstab
        echo -e "Swap was created successfully! \n"
    else
        echo -e "Swap Operation not permitted! [Not and Error] This is due to VPS provider \a"
        rm /var/swap.img
    fi
  fi
echo '==========================================================================='
}
function download(){

 echo -e "Prepare to download $COIN_NAME"
        cd ~
        wget -q ${COIN_BIN} > /dev/null 2>&1
        COIN_ZIP=$(echo ${COIN_BIN} | awk -F'/' '{print $NF}')
        tar -xvf ${COIN_ZIP} --strip-components 4 > /dev/null 2>&1
# delete excess files
        rm ~/lib* > /dev/null 2>&1
        rm ~/bench_scribe > /dev/null 2>&1
        rm ~/test_scribe > /dev/null 2>&1
        rm ~/scribeconsensus.h > /dev/null 2>&1
        rm ~/._* > /dev/null 2>&1
        rm -rf pkgconfig/ > /dev/null 2>&1
        cp ${COIN}{d,-cli} /usr/local/bin > /dev/null 2>&1
        rm ${COIN}* > /dev/null 2>&1
  chmod +x /usr/local/bin/${COIN}d > /dev/null 2>&1
  chmod +x /usr/local/bin/${COIN}-cli > /dev/null 2>&1
 # clear
}
function sentinel(){
#1. Install Prerequisites
#Make sure Python version 2.7.x or above is installed:

#python --version
#Update system packages and ensure virtualenv is installed:

        apt-get -yq update > /dev/null 2>&1

        apt-get -y install python-virtualenv > /dev/null 2>&1

#2. Install Sentinel
#Clone the Sentinel repo and install Python dependencies.
cd ${BASE}/${COIN}/.${COIN}core/ > /dev/null 2>&1

git clone https://github.com/scribenetwork/sentinel.git && cd sentinel > /dev/null 2>&1

sed -i 's/#scribe_conf/scribe_conf/g' /home/scribe/.scribecore/sentinel/sentinel.conf


virtualenv ./venv  > /dev/null 2>&1
./venv/bin/pip install -r requirements.txt  > /dev/null 2>&1

crontab -l | { cat; echo "* * * * * cd /home/scribe/.scribecore/sentinel && ./venv/bin/python bin/sentinel.py >/dev/null 2>&1"; } | crontab -
echo '- crontab updated for sentinel'
echo 'Sentinel installed'
}
function configure(){
# Check and ADDUSER .
echo 'Checking if '$COIN' user account exists'
        if getent passwd | grep $COIN >>/dev/null; then
                echo "User exists already...GOOD"
        else
                echo "User directories being created"
        adduser --disabled-password --gecos "" ${COIN} > /dev/null 2>&1
        echo 'Creating data directory '${BASE}'/'${COIN}'/.'${COIN}'core/'
        mkdir -p ${BASE}/${COIN} > /dev/null 2>&1
         mkdir -p ${BASE}/${COIN}/.${COIN}core > /dev/null 2>&1

#SET RPC PASSWORD
    echo 'Generating random password for '${COIN}' JSON RPC'
    USER=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    USERPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    Mkey=''
    echo 'Remember to set your masternode key in '${COIN}' config file'
sleep 5
#   read -e -p "[press enter for blank] MasterNode Key: " $MKey
#MAKE CONF
echo "Generating "${COIN}".conf"
echo "rpcallowip=127.0.0.1
rpcuser=$USER
rpcpassword=$USERPASS
server=1
daemon=1
listen=1
maxconnections=8
# set printtoconsole=1 to disable debug.log
printtoconsole=0
masternode=1
masternodeprivkey=
promode=1
bind=$IP:$PORT
rpcport=8001" |tee -a ${BASE}/${COIN}/.${COIN}core/${COIN}.conf > /dev/null 2>&1
echo "Configure complete"
fi
}

function systemconfig(){
#CREATE SYSTEMD FILE(S)
echo "Creating Systemd file"
echo "
[Unit]
Description=${COIN_NAME} service
After=network.target

[Service]
User=root
Group=root
Type=forking

ExecStart=$COIN_DAEMON -daemon -conf=${BASE}/${COIN}/.${COIN}core/$COIN.conf -datadir=${BASE}/${COIN}/.${COIN}core/
ExecStop=-$COIN_CLI -conf=${BASE}/${COIN}/.${COIN}core/$COIN.conf -datadir=${BASE}/${COIN}/.${COIN}core/ stop

Restart=always
PrivateTmp=true
TimeoutStopSec=60s
TimeoutStartSec=10s
StartLimitInterval=120s
StartLimitBurst=10
[Install]
WantedBy=multi-user.target" |tee -a /etc/systemd/system/${COIN}.service  > /dev/null 2>&1

systemctl daemon-reload > /dev/null 2>&1
systemctl enable ${COIN} > /dev/null 2>&1

}

install_deps
mkswap
configure
systemconfig
sleep 2
sentinel
sleep 2
download
echo ${COIN_NAME}" Install Complete"
