#!/bin/bash
# script developed by Dasche.

#where you want the coin to be installed 
# Edit these variables to get proper install. 

BASE='/home/capra/'
PORT=25791
COIN_REPO='https://github.com/nashsclay/capracoin/releases/download/v1.0/capracoin-mn-setup.tar.gz'
COINFILE='capracoin-mn-setup.tar.gz'
TMP_FOLDER=$(mktemp -d)
CONFIG_FILE='capra.conf'
COIN_NAME='Capra'
COIN_DAEMON='/usr/local/bin/capracoind'
COIN_CLI='/usr/local/bin/capracoin-cli'
COIN='capra'

# Execute options
ARGS=$(getopt -o "hp:n:c:r:wsudx" -l "help,count:,net" -n "multinode_$COIN.sh" -- "$@");
#edit these to set network to ipv4 (net=4)
net=4
#******************
# edit for count of number of masternodes to install.
count=5
# ******************

eval set -- "$ARGS";
#take input from command line
while true; do
    case "$1" in
#set variable for -n or --net input
        -n|--net)
            shift;
                    if [ -n "$1" ];
                    then
                        net="$1";
                        shift;
                    fi
            ;;
#set variable for -c or --count input
        -c|--count)
            shift;
                    if [ -n "$1" ];
                    then
                        count="$1";
                        shift;
                    fi
            ;;
        --)
           shift;
            break;
            ;;
   esac done
# if no arguments are sent continue
if [ $count == 0 ]; then
        count=1
fi
#######################-------IP TESTING
# break here of net detection is invalid
if [ ${net} -ne 4 ] && [ ${net} -ne 6 ]; then
    echo "invalid NETWORK setting, can only be 4 or 6!"
    exit 1;
fi
# use IPv4
if [ ${net} = 4 ]; then
        IPADDRESS=$(ip addr | grep 'inet ' | grep -Ev 'inet 127|inet 192\.168|inet 10\.' | sed "s/[[:space:]]*inet \([0-9.]*\)\/.*/\1/")
fi
# use IPv6 {this is broken} do not use ipv6
if [ ${net} = 6 ]; then
        IPADDRESS=$(ip -6 addr show dev eth0 | grep inet6 | awk -F '[ \t]+|/' '{print $3}' | grep -v ^fe80 | grep -v ^::1 | cut -f1-4
-d':' | head -1)
fi
#######################-------------------------------------------------------------------------END IP TESTING
# currently only for Ubuntu 16.04 & 18.04
    if [[ -r /etc/os-release ]]; then
        . /etc/os-release
        if [[ "${VERSION_ID}" != "16.04" ]] && [[ "${VERSION_ID}" != "18.04" ]] ; then
            echo "This script only supports Ubuntu 16.04 & 18.04 LTS, exiting."
            exit 1
        fi
    else
        # no, thats not ok!
        echo "This script only supports Ubuntu 16.04 & 18.04 LTS, exiting."
        exit 1
fi


#Check Deps

#apt-get install lshw

if [ -d "/var/lib/fail2ban/" ]; then
    echo -e "Dependencies already installed..."
    else
    echo -e "Updating system and installing required packages..."
 DEBIAN_FRONTEND=noninteractive
 apt-get -y update
  apt-get -y autoremove
  apt-get -y install wget nano htop jq
  apt-get install unzip
echo 'Installing and starting fail2ban anti-hacking system'
 apt-get -y install fail2ban
 service fail2ban restart
fi

#Create 4GB swap file
echo '==========================================================================='
echo "*** Testing for Swap file or partition"
    if grep -q "SwapTotal" /proc/meminfo; then
    echo -e "[SWAP FILE OK]Skipping disk swap configuration... \n"
    else
    echo "There is no swap file \n"
    echo -e "Creating 4GB disk swap file. \nThis may take a few minutes! \a"
    touch /var/swap.img
    chmod 600 swap.img
    dd if=/dev/zero of=/var/swap.img bs=1024k count=4000
    mkswap /var/swap.img 2> /dev/null
    swapon /var/swap.img 2> /dev/null
    if [ $? -eq 0 ]; then
        echo '/var/swap.img none swap sw 0 0' >> /etc/fstab
        echo -e "Swap was created successfully! \n"
    else
        echo -e "Swap Operation not permitted! \a"
        rm /var/swap.img
    fi
  fi
echo '==========================================================================='
#DOWNLOAD--------------------------------- Download Latest
echo 'Downloading and installing '$COIN_NAME' masternode files.. '
sleep 1
echo -ne '[#####                  ]   (33%)\r'
sleep 1

 wget -q $COIN_REPO &> /dev/null
 tar -xzf $COINFILE
 cp ${COIN}coin{d,-cli} /usr/local/bin &> /dev/null
 rm ${COIN}coin{d,-cli,-tx}
 rm $COINFILE

echo -ne '[#############          ]   (66%)\r'
sleep 1

 strip $COIN_DAEMON $COIN_CLI &> /dev/null
 cd ~ &> /dev/null
 chmod +x /usr/local/bin/${COIN}d &> /dev/null
 chmod +x /usr/local/bin/${COIN}-cli &> /dev/null

echo -ne '[#######################]   (100%)\r'
echo -ne '\n'
sleep 1

echo $COIN' Masternode files are now installed'
echo '==========================================================================='

# Check and ADDUSER .
echo 'checking if $COIN user account exists'
        if getent passwd | grep $COIN >>/dev/null; then
                echo "user exists already...GOOD"
        else
                echo "user being created"
        adduser --disabled-password --gecos "" $COIN
fi
#SETUP NETWORK
read IFACE <<< $(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}')
echo $IFACE


#
echo "* Creating masternode directories"
mkdir -p $BASE
for num in $(seq 1 $count); do
    if [ ! -d $BASE"$COIN"$num ]; then
        echo 'creating data directory '${BASE}$COIN$num
        mkdir -p ${BASE}$COIN$num
#SET RPC PASSWORD
        echo 'Generating Random Password for $COINd JSON RPC'
        USER=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
        USERPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
        echo 'Enter your masternode private key for $COIN Masternode#'$num
                read -e -p "[press enter for blank] MasterNode Key: " MKey
#MAKE CONF
echo 'Generating '$COIN'.conf'
echo "rpcallowip=127.0.0.1
rpcuser=$USER
rpcpassword=$USERPASS
server=1
daemon=1
listen=1
maxconnections=12
masternode=1
masternodeprivkey=$MKey
promode=1
bind=192.168.1.$num:$PORT
rpcport=500$num" |tee -a ${BASE}${COIN}$num/${COIN}.conf

#CREATE SYSTEMD FILE(S)
echo "[Unit]
Description=${COIN}$num service
After=network.target

[Service]
User=root
Group=root
Type=forking

ExecStart=$COIN_DAEMON -daemon -conf=${BASE}${COIN}$num/$COIN.conf -datadir=${BASE}${COIN}$num/
ExecStop=-$COIN_CLI -conf=${BASE}${COIN}$num/$COIN.conf -datadir=${BASE}${COIN}$num stop

Restart=always
PrivateTmp=true
TimeoutStopSec=60s
TimeoutStartSec=10s
StartLimitInterval=120s
StartLimitBurst=5
[Install]
WantedBy=multi-user.target" |tee -a /etc/systemd/system/${COIN}$num.service

#systemctl enable $COIN$num.service
echo '[*]systemctl configured for autoboot for '${COIN}$num'.service'

echo 'Masternode '$num >> /etc/network/interfaces.tail
echo '  up   ip addr add 192.168.1.'$num'/24 dev '$IFACE' label '$IFACE':'$num >> /etc/network/interfaces.tail
echo '  down ip addr del 192.168.1.'$num'/24 dev '$IFACE' label '$IFACE':'$num >> /etc/network/interfaces.tail
systemctl enable tour${num}.service

fi
done

chown -R $COIN:$COIN $BASE
chmod -R g=u $BASE
echo 'Script complete, nodes will not be started automaticly, check systemctrl start '${COIN}$num'.sevice'
#restart networking
ifdown $IFACE && ifup $IFACE